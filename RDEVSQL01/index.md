#### 

[Project](../index.md) > RDEVSQL01

# ![Server](../Images/ntServer.png) RDEVSQL01

---

## <a name="#databases"></a>Databases (1)

* ![Database](../Images/Database.png) [RangerPOS](User_databases/RangerPOS/index.md)


---

## <a name="#serverproperties"></a>Server Properties

| Property | Value |
|---|---|
| Product | Microsoft SQL Server |
| Version | 15.0.2000.5 |
| Language | English (United States) |
| Platform | NT x64 |
| Edition | Developer Edition (64-bit) |
| Engine Edition | 3 (Enterprise) |
| Processors | 1 |
| OS Version | 6.3 (17763) |
| Physical Memory | 13633 |
| Is Clustered | False |
| Root Directory | C:\\Program Files\\Microsoft SQL Server\\MSSQL15.MSSQLSERVER\\MSSQL |
| Collation | SQL_Latin1_General_CP1_CI_AS |


---

## <a name="#serversettings"></a>Server Settings

| Property | Value |
|---|---|
| Default data file path | C:\\Program Files\\Microsoft SQL Server\\MSSQL11.MSSQLSERVER\\MSSQL\\DATA\\ |
| Default backup file path | C:\\Program Files\\Microsoft SQL Server\\MSSQL11.MSSQLSERVER\\MSSQL\\Backup |
| Default log file path | C:\\Program Files\\Microsoft SQL Server\\MSSQL11.MSSQLSERVER\\MSSQL\\DATA\\ |
| Recovery Interval (minutes) | 0 |
| Default index fill factor | 0 |
| Default backup media retention | 0 |
| Compress Backup | NO |


---

## <a name="#advancedserversettings"></a>Advanced Server Settings

| Property | Value |
|---|---|
| Full text upgrade option | 2 |
| Locks | 0 |
| Nested triggers enabled | YES |
| Allow triggers to fire others | YES |
| Default language | English |
| Network packet size | 4096 |
| Default fulltext language LCID | 1033 |
| Two-digit year cutoff | 2049 |
| Remote login timeout | 10 |
| Cursor threshold | -1 |
| Max text replication size | 65536 |
| Parallelism cost threshold | 5 |
| Max degree of parallelism | 0 |
| Min server memory | 16 |
| Max server memory | 2147483647 |
| Scan for startup procs | NO |
| Transform noise words | NO |
| CLR enabled | NO |
| Blocked process threshold | 0 |
| Filestream access level | NO |
| Optimize for ad hoc workloads | NO |
| CLR strict security | YES |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

