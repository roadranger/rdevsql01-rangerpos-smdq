#### 

[Project](../../../index.md) > [RDEVSQL01](../../index.md) > [User databases](../index.md) > RangerPOS

# ![Database](../../../Images/ntDatabase.png) RangerPOS Database

---

<a name="#description">MS_Description</a>

Operational and transaction data for Road Ranger POS systems

## <a name="#objecttypes">Object Types</a>

* ![Tables](../../../Images/Table.png) [Tables](Tables/Tables.md)


---

## <a name="#dbproperties"></a>Database Properties

| Property | Value |
|---|---|
| SQL Server Version | Max |
| Compatibility Level | SQL Server 2008 |
| Last backup time | 09/24/2019 |
| Last log backup time | 09/24/2019 |
| Creation date | Sep 25 2019  |
| Users | 24 |
| Database Encryption Enabled | NO |
| Database Encryption Algorithm | None |
| Database size | 650001.00 MB |
| Unallocated space | 1993.70 MB |


---

## <a name="#dboptions"></a>Database Options

| Property | Value |
|---|---|
| Compatibility Level | 100 |
| Database collation | SQL_Latin1_General_CP1_CI_AS |
| Restrict access | MULTI_USER |
| Is read-only | NO |
| Auto close | NO |
| Auto shrink | NO |
| Database status | ONLINE |
| In standby | NO |
| Cleanly shutdown | NO |
| Supplemental logging enabled | NO |
| Snapshot isolation state | OFF |
| Read committed snapshot on | NO |
| Recovery model | SIMPLE |
| Page verify option | CHECKSUM |
| Auto create statistics | YES |
| Auto update statistics | YES |
| Auto update statistics asynchronously | NO |
| ANSI NULL default | NO |
| ANSI NULL enabled | NO |
| ANSI padding enabled | NO |
| ANSI warnings enabled | NO |
| Arithmetic abort enabled | NO |
| Concatenating NULL yields NULL | NO |
| Numeric roundabort enabled | NO |
| Quoted Identifier On | NO |
| Recursive triggers enabled | NO |
| Close cursors on commit | NO |
| Local cursors by default | NO |
| Fulltext enabled | NO |
| Trustworthy | NO |
| Database chaining | NO |
| Forced parameterization | NO |
| Master key encrypted by server | NO |
| Published | NO |
| Subscribed | NO |
| Merge published | NO |
| Is distribution database | NO |
| Sync with backup | NO |
| Service broker GUID | 649f3fde-a6e4-4a2d-8c0b-8a7b53c62497 |
| Service broker enabled | NO |
| Log reuse wait | NOTHING |
| Date correlation | NO |
| CDC enabled | NO |
| Encrypted | False |
| Honor broker priority | False |
| Default language | English |
| Default fulltext language LCID | 1033 |
| Nested triggers enabled | YES |
| Transform noise words | NO |
| Two-digit year cutoff | 2049 |
| Containment | NONE |
| Target recovery time | 0 |
| Database owner | RDRANGER\\dderry |


---

## <a name="#files"></a>Files

| Name | Type | Size | Maxsize | Autogrowth | File Name |
|---|---|---|---|---|---|
| RangerPOS | Data | 633.79 GB | 1464.84 GB | 1000.00 MB | G:\\dbs\\RangerPOS.mdf |
| RangerPOS_log | Log | 0.98 GB | 97.66 GB | 1000.00 MB | H:\\logs\\RangerPOS_log.ldf |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

