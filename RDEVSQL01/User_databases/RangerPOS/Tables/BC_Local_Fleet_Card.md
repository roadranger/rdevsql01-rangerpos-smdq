#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Local_Fleet_Card

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Local_Fleet_Card]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Local_Fleet_Card: Card_ID](../../../../Images/pkcluster.png)](#indexes) | Card_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Method_ID | int | 4 | NULL allowed |  |
|  | Card_Code | varchar(2) | 2 | NULL allowed |  |
|  | Prompt_Start_Location | smallint | 2 | NULL allowed |  |
|  | Prompt_Length | smallint | 2 | NULL allowed |  |
|  | Prompt_Value | varchar(5) | 5 | NULL allowed |  |
|  | Swiped_Prompt_Code | varchar(5) | 5 | NULL allowed |  |
|  | Manual_Prompt_Code | varchar(5) | 5 | NULL allowed |  |
|  | Prompt_Right_Length | smallint | 2 | NULL allowed |  |
|  | Prompt_Right_Value | varchar(5) | 5 | NULL allowed |  |
|  | Product_Restrict_Start | smallint | 2 | NULL allowed |  |
|  | Product_Restrict_Length | smallint | 2 | NULL allowed |  |
|  | Product_Restrict_Code | varchar(5) | 5 | NULL allowed |  |
|  | Swipe_Restrict_Action | smallint | 2 | NULL allowed |  |
|  | Manual_Restrict_Action | smallint | 2 | NULL allowed |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

