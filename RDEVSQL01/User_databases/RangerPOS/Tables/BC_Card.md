#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Card

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Card]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Card: Card_Type_ID](../../../../Images/pkcluster.png)](#indexes) | Card_Type_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Method_ID | int | 4 | NULL allowed |  |
|  | Card_Code | varchar(20) | 20 | NULL allowed |  |
|  | Payment_Type | varchar(2) | 2 | NULL allowed |  |
|  | Sale_Code | varchar(2) | 2 | NULL allowed |  |
|  | Void_Sale_Code | varchar(2) | 2 | NULL allowed |  |
|  | Return_Code | varchar(2) | 2 | NULL allowed |  |
|  | Completion_Code | varchar(2) | 2 | NULL allowed |  |
|  | Void_Return_Code | varchar(2) | 2 | NULL allowed |  |
|  | Pre_Auth_Code | varchar(2) | 2 | NULL allowed |  |
|  | Check_Exp_Date | bit | 1 | NULL allowed |  |
|  | Is_Active | bit | 1 | NULL allowed |  |
|  | Pre_Auth_Amount | decimal(8,2) | 5 | NULL allowed |  |
|  | Pump_Limit | decimal(8,2) | 5 | NULL allowed |  |
|  | Allow_Standin | bit | 1 | NULL allowed |  |
|  | StandIn_Limit | decimal(8,2) | 5 | NULL allowed |  |
|  | Commercial_Card_Code | varchar(2) | 2 | NULL allowed |  |
|  | Sale_Cashback_Code | varchar(2) | 2 | NULL allowed |  |
|  | Void_Sale_Cashback_Code | varchar(2) | 2 | NULL allowed |  |
|  | Information_Update_Code | varchar(2) | 2 | NULL allowed |  |
|  | Inside_Pre_Auth_Code | varchar(2) | 2 | NULL allowed |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

