#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Product_Code_Translation

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Product_Code_Translation]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Product_Code_Translation: Translation_ID](../../../../Images/pkcluster.png)](#indexes) | Translation_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Bill_Comp_ID | int | 4 | NULL allowed |  |
|  | Method_ID | int | 4 | NULL allowed |  |
|  | PCATS_Code | varchar(6) | 6 | NULL allowed |  |
|  | Bill_Comp_Product_Code | varchar(50) | 50 | NULL allowed |  |
|  | Reefer_Product_Code | varchar(50) | 50 | NULL allowed |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

