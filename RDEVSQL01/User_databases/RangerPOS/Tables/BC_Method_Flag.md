#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Method_Flag

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Method_Flag]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Method_Flag: Method_Flag_ID](../../../../Images/pkcluster.png)](#indexes) | Method_Flag_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Method_ID | int | 4 | NULL allowed |  |
|  | Flag_Value | varchar(1000) | 1000 | NULL allowed |  |
|  | Flag_Num | smallint | 2 | NULL allowed |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

