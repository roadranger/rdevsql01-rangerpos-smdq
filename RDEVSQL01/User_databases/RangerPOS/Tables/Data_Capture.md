#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.Data_Capture

# ![Tables](../../../../Images/Table32.png) [dbo].[Data_Capture]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Default |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_Data_Capture: Capture_ID](../../../../Images/pkcluster.png)](#indexes) | Capture_ID | int | 4 | NOT NULL | 1 - 1 |  |
|  | Capture_Num | int | 4 | NULL allowed |  |  |
|  | Capture_Name | varchar(15) | 15 | NULL allowed |  |  |
|  | Capture_Long_Name | varchar(30) | 30 | NULL allowed |  |  |
|  | Capture_Max_Len | smallint | 2 | NULL allowed |  |  |
|  | May_Be_Local | bit | 1 | NOT NULL |  | ((0)) |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |  |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | EOD_ID | int | 4 | NOT NULL |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

