#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_Payment_Batch_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_Payment_Batch_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_Payment_Batch_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Payment_Batch_ID | int | 4 | NULL allowed |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |
|  | POS_Num | int | 4 | NOT NULL |  |  |
|  | Batch_Num | varchar(30) | 30 | NOT NULL |  |  |
|  | Shift_Close | bit | 1 | NULL allowed |  |  |
|  | Batch_Limit_Close | bit | 1 | NULL allowed |  |  |
|  | Batch_Closed | bit | 1 | NULL allowed |  |  |
|  | Control_Num | varchar(30) | 30 | NULL allowed |  |  |
|  | Host_Date | varchar(8) | 8 | NULL allowed |  |  |
|  | Host_Time | varchar(6) | 6 | NULL allowed |  |  |
|  | Host_Response_Literal | varchar(100) | 100 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

