#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_Shift_Detail_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_Shift_Detail_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_Shift_Detail_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Department_Number | int | 4 | NULL allowed |  |  |
|  | Shift_Detail_ID | int | 4 | NOT NULL |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |
|  | POS_Num | smallint | 2 | NULL allowed |  |  |
|  | Shift_Num | smallint | 2 | NULL allowed |  |  |
|  | User_ID | varchar(36) | 36 | NULL allowed |  |  |
|  | Detail_Date_Time | datetime | 8 | NULL allowed |  |  |
|  | Receipt_Num | smallint | 2 | NULL allowed |  |  |
|  | Account_Name | varchar(15) | 15 | NULL allowed |  |  |
|  | Account_ID | int | 4 | NULL allowed |  |  |
|  | Acct_Type_ID | int | 4 | NULL allowed |  |  |
|  | Product_ID | int | 4 | NULL allowed |  |  |
|  | Description | varchar(70) | 70 | NULL allowed |  |  |
|  | Price | money | 8 | NULL allowed |  |  |
|  | Quantity | decimal(10,4) | 9 | NULL allowed |  |  |
|  | Amount | float | 8 | NULL allowed |  |  |
|  | Pump_Num | tinyint | 1 | NULL allowed |  |  |
|  | Pump_Customer_Num | tinyint | 1 | NULL allowed |  |  |
|  | Payment_Mode_ID | tinyint | 1 | NULL allowed |  |  |
|  | Service_Mode_ID | smallint | 2 | NULL allowed |  |  |
|  | Trans_Num | int | 4 | NULL allowed |  |  |
|  | Birth_Date | varchar(10) | 10 | NULL allowed |  |  |
|  | Adjustment_Amount | money | 8 | NULL allowed |  |  |
|  | Price_Override | bit | 1 | NULL allowed |  |  |
|  | Product_Capture | bit | 1 | NULL allowed |  |  |
|  | Shift_Detail_GUID | bit | 1 | NULL allowed |  |  |
|  | Reefer | smallint | 2 | NULL allowed |  |  |
|  | Credit_Price | money | 8 | NULL allowed |  |  |
|  | Cash_Price | money | 8 | NULL allowed |  |  |
|  | Product_UPC | varchar(16) | 16 | NULL allowed |  |  |
|  | PCATS_Code | varchar(6) | 6 | NULL allowed |  |  |
|  | Car_Wash_Code | char(6) | 6 | NULL allowed |  |  |
|  | Car_Wash_Exp | char(6) | 6 | NULL allowed |  |  |
|  | Orig_Quantity | decimal(8,4) | 5 | NULL allowed |  |  |
|  | Orig_Amount | money | 8 | NULL allowed |  |  |
|  | UPC_Format | varchar(16) | 16 | NULL allowed |  |  |
|  | POS_Code_Modifier | varchar(4) | 4 | NULL allowed |  |  |
|  | Single_Price | money | 8 | NULL allowed |  |  |
|  | Pack_Size | tinyint | 1 | NULL allowed |  |  |
|  | Promotion_Discount | money | 8 | NULL allowed |  |  |
|  | Loyalty_Reward_ID | varchar(40) | 40 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

