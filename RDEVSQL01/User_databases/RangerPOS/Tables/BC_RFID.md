#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_RFID

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_RFID]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_RFID: BC_RFID_ID](../../../../Images/pkcluster.png)](#indexes) | BC_RFID_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Bill_Comp_ID | int | 4 | NOT NULL |  |
|  | Method_ID | int | 4 | NOT NULL |  |
|  | Start_Code | varchar(20) | 20 | NOT NULL |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

