#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Method_Fuel_Discounts

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Method_Fuel_Discounts]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability |
|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Method_Fuel_Discounts: Method_ID\Grade_Num](../../../../Images/pkcluster.png)](#indexes) | Method_ID | int | 4 | NOT NULL |
| [![Cluster Primary Key PK_BC_Method_Fuel_Discounts: Method_ID\Grade_Num](../../../../Images/pkcluster.png)](#indexes) | Grade_Num | smallint | 2 | NOT NULL |
|  | Discount | money | 8 | NOT NULL |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |
|  | LocNum | int | 4 | NULL allowed |
|  | EOD_ID | int | 4 | NOT NULL |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

