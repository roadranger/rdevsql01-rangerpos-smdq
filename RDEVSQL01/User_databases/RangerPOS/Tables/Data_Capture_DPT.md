#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.Data_Capture_DPT

# ![Tables](../../../../Images/Table32.png) [dbo].[Data_Capture_DPT]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_Data_Capture_DPT: DPT_Capture_ID](../../../../Images/pkcluster.png)](#indexes) | DPT_Capture_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Capture_Num | int | 4 | NULL allowed |  |
|  | Capture_Name | varchar(30) | 30 | NULL allowed |  |
|  | DPT_Data_Code | varchar(1) | 1 | NULL allowed |  |
|  | DPT_Prompt_Code | varchar(1) | 1 | NULL allowed |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

