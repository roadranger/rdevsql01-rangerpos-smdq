#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.Account_Shift_Balance

# ![Tables](../../../../Images/Table32.png) [dbo].[Account_Shift_Balance]

---

## <a name="#columns"></a>Columns

| Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|
| Id | int | 4 | NOT NULL | 1 - 1 | NO |
| LocNum | int | 4 | NOT NULL |  |  |
| ElsDate | smalldatetime | 4 | NOT NULL |  |  |
| pos_staging_upload_id | int | 4 | NULL allowed |  |  |
| Shift_Bal_ID | int | 4 | NOT NULL |  |  |
| Shift_Bal_Name | varchar(20) | 20 | NULL allowed |  |  |
| Shift_Bal_Add_Sub | smallint | 2 | NULL allowed |  |  |
| Shift_Bal_Order | smallint | 2 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

