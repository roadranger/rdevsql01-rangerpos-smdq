#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_Shift_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_Shift_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_Shift_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Shift_Id | int | 4 | NOT NULL |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |
|  | POS_Num | smallint | 2 | NULL allowed |  |  |
|  | Shift_Num | smallint | 2 | NULL allowed |  |  |
|  | Cashier_Name | varchar(50) | 50 | NULL allowed |  |  |
|  | Shift_Start_Date | datetime | 8 | NULL allowed |  |  |
|  | Shift_End_Date | datetime | 8 | NULL allowed |  |  |
|  | Shift_Comments | varchar(250) | 250 | NULL allowed |  |  |
|  | Shift_GUID | bit | 1 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

