#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Method_Prompt

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Method_Prompt]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Method_Prompt: Prompt_ID](../../../../Images/pkcluster.png)](#indexes) | Prompt_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Method_ID | int | 4 | NULL allowed |  |
|  | Capture_Num | int | 4 | NULL allowed |  |
|  | Is_Prompt_Required | bit | 1 | NULL allowed |  |
|  | Prompt_Min_Len | smallint | 2 | NULL allowed |  |
|  | Prompt_Max_Len | smallint | 2 | NULL allowed |  |
|  | Prompt_Input_Type | varchar(10) | 10 | NULL allowed |  |
|  | Mask_Input | bit | 1 | NULL allowed |  |
|  | Prompt_Pin_Pad | bit | 1 | NULL allowed |  |
|  | Print_Prompt | bit | 1 | NULL allowed |  |
|  | Init_Before_Preauth | bit | 1 | NULL allowed |  |
|  | Use_EFT | bit | 1 | NULL allowed |  |
|  | Pin_Pad_Prompt_Text | varchar(25) | 25 | NULL allowed |  |
|  | Manual_Card_Entry | bit | 1 | NULL allowed |  |
|  | Screen_Order | smallint | 2 | NULL allowed |  |
|  | Is_Disabled | bit | 1 | NULL allowed |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

