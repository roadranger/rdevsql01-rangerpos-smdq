#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > Tables

# ![Tables](../../../../Images/Table32.png) Tables

---

## <a name="#objects"></a>Objects

| Name | Description |
|---|---|
| [dbo.Account](Account.md) | _SMDQ Account records by location from store level TRENDAR tables for reporting on History_*_ |
| [dbo.Account_Blackouts](Account_Blackouts.md) | _SMDQ Account_Blackouts records by location from store level TRENDAR tables for reporting on History_*_ |
| [dbo.Account_Group](Account_Group.md) |  |
| [dbo.Account_Shift_Balance](Account_Shift_Balance.md) |  |
| [dbo.Account_Summary](Account_Summary.md) |  |
| [dbo.Account_Type](Account_Type.md) |  |
| [dbo.BC_Card](BC_Card.md) |  |
| [dbo.BC_Card_Bin](BC_Card_Bin.md) |  |
| [dbo.BC_Card_Bin_Range](BC_Card_Bin_Range.md) |  |
| [dbo.BC_Company](BC_Company.md) |  |
| [dbo.BC_Company_Flag](BC_Company_Flag.md) |  |
| [dbo.BC_Local_Fleet_Card](BC_Local_Fleet_Card.md) |  |
| [dbo.BC_Method](BC_Method.md) |  |
| [dbo.BC_Method_Flag](BC_Method_Flag.md) |  |
| [dbo.BC_Method_Fuel_Discounts](BC_Method_Fuel_Discounts.md) |  |
| [dbo.BC_Method_Prompt](BC_Method_Prompt.md) |  |
| [dbo.BC_Method_Restrictions](BC_Method_Restrictions.md) |  |
| [dbo.BC_Product_Code_Translation](BC_Product_Code_Translation.md) |  |
| [dbo.BC_RFID](BC_RFID.md) |  |
| [dbo.Data_Capture](Data_Capture.md) |  |
| [dbo.Data_Capture_DPT](Data_Capture_DPT.md) |  |
| [dbo.Data_Flag](Data_Flag.md) |  |
| [dbo.History_Account_Summary_201910](History_Account_Summary_201910.md) |  |
| [dbo.History_EOD_201910](History_EOD_201910.md) |  |
| [dbo.History_EPS_Log_201910](History_EPS_Log_201910.md) |  |
| [dbo.History_FD_Data_201910](History_FD_Data_201910.md) |  |
| [dbo.History_FD_Product_201910](History_FD_Product_201910.md) |  |
| [dbo.History_FD_Sales_Tax_201910](History_FD_Sales_Tax_201910.md) |  |
| [dbo.History_FD_Transaction_201910](History_FD_Transaction_201910.md) |  |
| [dbo.History_Fuel_Deliveries_201910](History_Fuel_Deliveries_201910.md) |  |
| [dbo.History_Fuel_Inventory_201910](History_Fuel_Inventory_201910.md) |  |
| [dbo.History_Fuel_Inventory_Periods_201910](History_Fuel_Inventory_Periods_201910.md) |  |
| [dbo.History_Payment_Auth_Log_201910](History_Payment_Auth_Log_201910.md) |  |
| [dbo.History_Payment_Batch_201910](History_Payment_Batch_201910.md) |  |
| [dbo.History_Payment_Batch_Host_Totals_201910](History_Payment_Batch_Host_Totals_201910.md) |  |
| [dbo.History_Payment_Data_201910](History_Payment_Data_201910.md) |  |
| [dbo.History_Payment_Signature_201910](History_Payment_Signature_201910.md) |  |
| [dbo.History_Payment_Transaction_201910](History_Payment_Transaction_201910.md) |  |
| [dbo.History_Product_Summary_201910](History_Product_Summary_201910.md) |  |
| [dbo.History_Shift_201910](History_Shift_201910.md) |  |
| [dbo.History_Shift_Detail_201910](History_Shift_Detail_201910.md) |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

