#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_Payment_Transaction_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_Payment_Transaction_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_Payment_Transaction_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Payment_Tran_ID | int | 4 | NULL allowed |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |
|  | Shift_Num | smallint | 2 | NULL allowed |  |  |
|  | Trans_Num | smallint | 2 | NULL allowed |  |  |
|  | Auth_Date_Time | datetime | 8 | NULL allowed |  |  |
|  | Account_ID | int | 4 | NULL allowed |  |  |
|  | Amount | money | 8 | NULL allowed |  |  |
|  | Card_Code | varchar(20) | 20 | NULL allowed |  |  |
|  | Track_Data_Masked | varchar(20) | 20 | NULL allowed |  |  |
|  | Seq_Num | int | 4 | NULL allowed |  |  |
|  | Payment_Type | varchar(15) | 15 | NULL allowed |  |  |
|  | Keyed_Manual | bit | 1 | NULL allowed |  |  |
|  | Manual_Authorization | bit | 1 | NULL allowed |  |  |
|  | Manual_Auth_Code | varchar(15) | 15 | NULL allowed |  |  |
|  | Gift_Card_Balance | money | 8 | NULL allowed |  |  |
|  | Debit_Card_Balance | money | 8 | NULL allowed |  |  |
|  | Payment_Num | smallint | 2 | NULL allowed |  |  |
|  | Exp_Date | varchar(200) | 200 | NULL allowed |  |  |
|  | Bill_Comp_Code | varchar(20) | 20 | NULL allowed |  |  |
|  | Method_Code | varchar(15) | 15 | NULL allowed |  |  |
|  | Status | smallint | 2 | NULL allowed |  |  |
|  | Payment_Tran_GUID | bit | 1 | NULL allowed |  |  |
|  | DB_Or_Funded | smallint | 2 | NULL allowed |  |  |
|  | Cash_Back_Amount | money | 8 | NULL allowed |  |  |
|  | Terminal_Batch | varchar(20) | 20 | NULL allowed |  |  |
|  | Terminal_ID | varchar(30) | 30 | NULL allowed |  |  |
|  | Acquirer_ID | varchar(20) | 20 | NULL allowed |  |  |
|  | Acquirer_Batch | varchar(20) | 20 | NULL allowed |  |  |
|  | Payment_Method | varchar(20) | 20 | NULL allowed |  |  |
|  | Card_Circuit | varchar(20) | 20 | NULL allowed |  |  |
|  | Time_Display | bit | 1 | NULL allowed |  |  |
|  | Loyalty_Allowed | bit | 1 | NULL allowed |  |  |
|  | Suppress_Unit_Price | bit | 1 | NULL allowed |  |  |
|  | Request_ID | varchar(40) | 40 | NULL allowed |  |  |
|  | Workstation_ID | varchar(20) | 20 | NULL allowed |  |  |
|  | Trans_Code | varchar(2) | 2 | NULL allowed |  |  |
|  | Return_Flag | bit | 1 | NULL allowed |  |  |
|  | Retrieval_Data | varchar(50) | 50 | NULL allowed |  |  |
|  | Retain_Data | varchar(100) | 100 | NULL allowed |  |  |
|  | Purchase_Card_Indicator | varchar(2) | 2 | NULL allowed |  |  |
|  | Card_Scanned | bit | 1 | NULL allowed |  |  |
|  | Signature | varchar(5632) | 5632 | NULL allowed |  |  |
|  | Card_BIN | varchar(6) | 6 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

