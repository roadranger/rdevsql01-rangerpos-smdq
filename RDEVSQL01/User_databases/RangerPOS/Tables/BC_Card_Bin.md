#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Card_Bin

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Card_Bin]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Description |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Card_Bin: Card_Bin_ID](../../../../Images/pkcluster.png)](#indexes) | Card_Bin_ID | int | 4 | NOT NULL | 1 - 1 |  |
|  | Bill_Comp_ID | int | 4 | NULL allowed |  |  |
|  | Method_ID | int | 4 | NULL allowed |  |  |
|  | Card_Code | varchar(2) | 2 | NULL allowed |  |  |
|  | Track2_Reg_Exp | varchar(200) | 200 | NULL allowed |  |  |
|  | Track1_Reg_Exp | varchar(200) | 200 | NULL allowed |  |  |
|  | Manual_Reg_Exp | varchar(200) | 200 | NULL allowed |  |  |
|  | Manual_Prefix | varchar(20) | 20 | NULL allowed |  |  |
|  | Manual_Offset | smallint | 2 | NULL allowed |  |  |
|  | Check_Digit | smallint | 2 | NULL allowed |  |  |
|  | Check_Start | smallint | 2 | NULL allowed |  |  |
|  | Check_End | smallint | 2 | NULL allowed |  |  |
|  | Second_Check_Digit | smallint | 2 | NULL allowed |  |  |
|  | Second_Check_Start | smallint | 2 | NULL allowed |  |  |
|  | Second_Check_End | smallint | 2 | NULL allowed |  |  |
|  | Card_Bin_Order | smallint | 2 | NULL allowed |  | _the order that regular expressions are checked_ |
|  | CStore_Flag | bit | 1 | NULL allowed |  | _determine if the card is allowed at a C-Store only terminals or a DPT_ |
|  | Consumer_Card | bit | 1 | NULL allowed |  | _This will determine if we are allowed to keep the track data or not_ |
|  | Exp_Date_Format | varchar(10) | 10 | NULL allowed |  |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |  |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | EOD_ID | int | 4 | NOT NULL |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

