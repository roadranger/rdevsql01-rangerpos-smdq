#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_Product_Summary_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_Product_Summary_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_Product_Summary_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Product_UPC | varchar(16) | 16 | NULL allowed |  |  |
|  | Hist_Prod_Sum_ID | int | 4 | NULL allowed |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |
|  | Product_ID | int | 4 | NULL allowed |  |  |
|  | Sales_Amount | money | 8 | NULL allowed |  |  |
|  | Sales_Qty | int | 4 | NULL allowed |  |  |
|  | Hist_Prod_Sum_GUID | bit | 1 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

