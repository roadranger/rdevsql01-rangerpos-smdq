#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.Account_Group

# ![Tables](../../../../Images/Table32.png) [dbo].[Account_Group]

---

## <a name="#columns"></a>Columns

| Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|
| Id | int | 4 | NOT NULL | 1 - 1 | NO |
| LocNum | int | 4 | NOT NULL |  |  |
| ElsDate | smalldatetime | 4 | NOT NULL |  |  |
| pos_staging_upload_id | int | 4 | NULL allowed |  |  |
| Acct_Group_ID | int | 4 | NOT NULL |  |  |
| Acct_Group_Name | varchar(20) | 20 | NULL allowed |  |  |
| Acct_Group_Order | smallint | 2 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

