#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Card_Bin_Range

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Card_Bin_Range]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Card_Bin_Range: Card_Bin_Range_ID](../../../../Images/pkcluster.png)](#indexes) | Card_Bin_Range_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Method_ID | int | 4 | NOT NULL |  |
|  | Bin_Begin | varchar(20) | 20 | NOT NULL |  |
|  | Bin_End | varchar(20) | 20 | NOT NULL |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

