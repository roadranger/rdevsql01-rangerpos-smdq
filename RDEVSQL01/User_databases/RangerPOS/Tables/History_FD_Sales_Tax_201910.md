#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_FD_Sales_Tax_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_FD_Sales_Tax_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_FD_Sales_Tax_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Tax_Rate | money | 8 | NULL allowed |  |  |
|  | Tax_Strategy_ID | int | 4 | NULL allowed |  |  |
|  | FD_Sales_Tax_ID | bigint | 8 | NULL allowed |  |  |
|  | Shift_Num | smallint | 2 | NOT NULL |  |  |
|  | Trans_Num | int | 4 | NOT NULL |  |  |
|  | Sales_Tax_ID | int | 4 | NOT NULL |  |  |
|  | Sales_Amount | money | 8 | NULL allowed |  |  |
|  | Tax_Amount | money | 8 | NULL allowed |  |  |
|  | Is_Refund | bit | 1 | NULL allowed |  |  |
|  | Is_Exempt | bit | 1 | NULL allowed |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |
|  | Account_ID | int | 4 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

