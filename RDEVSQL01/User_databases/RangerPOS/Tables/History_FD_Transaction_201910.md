#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_FD_Transaction_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_FD_Transaction_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_FD_Transaction_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Transaction_ID | int | 4 | NOT NULL |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |
|  | POS_Num | smallint | 2 | NULL allowed |  |  |
|  | Shift_Num | smallint | 2 | NULL allowed |  |  |
|  | Trans_Num | int | 4 | NULL allowed |  |  |
|  | Trans_Date_Time | datetime | 8 | NULL allowed |  |  |
|  | Account_Name | varchar(30) | 30 | NULL allowed |  |  |
|  | Account_City | varchar(25) | 25 | NULL allowed |  |  |
|  | Account_State | varchar(2) | 2 | NULL allowed |  |  |
|  | Account_Num | varchar(20) | 20 | NULL allowed |  |  |
|  | Hubometer | varchar(20) | 20 | NULL allowed |  |  |
|  | PO_Num | varchar(20) | 20 | NULL allowed |  |  |
|  | Truck_Num | varchar(20) | 20 | NULL allowed |  |  |
|  | Invoice_Num | varchar(20) | 20 | NULL allowed |  |  |
|  | Bill_Comp_Name | varchar(30) | 30 | NULL allowed |  |  |
|  | Customer_Name | varchar(30) | 30 | NULL allowed |  |  |
|  | Pay_Method | varchar(30) | 30 | NULL allowed |  |  |
|  | Trans_Status | varchar(20) | 20 | NULL allowed |  |  |
|  | Transaction_GUID | bit | 1 | NULL allowed |  |  |
|  | Status_Date_Time | datetime | 8 | NULL allowed |  |  |
|  | Account_Type | char(2) | 2 | NULL allowed |  |  |
|  | Daily_Rpt_Period | tinyint | 1 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

