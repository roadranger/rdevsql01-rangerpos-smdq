#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_Fuel_Inventory_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_Fuel_Inventory_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
| [![Cluster Primary Key PK_History_Fuel_Inventory_201910: Id](../../../../Images/pkcluster.png)](#indexes) | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
|  | Period_ID | int | 4 | NOT NULL |  |  |
|  | Tank | tinyint | 1 | NOT NULL |  |  |
|  | Grade_Num | smallint | 2 | NOT NULL |  |  |
|  | Opening_Volume | int | 4 | NOT NULL |  |  |
|  | Delivered_Volume | int | 4 | NOT NULL |  |  |
|  | Dispensed_Volume | int | 4 | NOT NULL |  |  |
|  | Calibration_Volume | int | 4 | NOT NULL |  |  |
|  | Closing_Volume | int | 4 | NOT NULL |  |  |
|  | Water_Level | decimal(3,1) | 5 | NOT NULL |  |  |
|  | Fuel_Volume_201 | int | 4 | NULL allowed |  |  |
|  | Fuel_TC_Volume_201 | int | 4 | NULL allowed |  |  |
|  | Ullage_201 | int | 4 | NULL allowed |  |  |
|  | Fuel_Height_201 | decimal(10,2) | 9 | NULL allowed |  |  |
|  | Water_Height_201 | decimal(10,2) | 9 | NULL allowed |  |  |
|  | Water_Volume_201 | decimal(6,2) | 5 | NULL allowed |  |  |
|  | Temperature_201 | decimal(5,2) | 5 | NULL allowed |  |  |
|  | Max_Volume_201 | int | 4 | NULL allowed |  |  |
|  | Ullage_95_Pct | int | 4 | NULL allowed |  |  |
|  | Ullage_90_Pct | int | 4 | NULL allowed |  |  |
|  | EOD_ID | int | 4 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

