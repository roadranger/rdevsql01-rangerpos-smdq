#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.History_Payment_Signature_201910

# ![Tables](../../../../Images/Table32.png) [dbo].[History_Payment_Signature_201910]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|---|
|  | Id | bigint | 8 | NOT NULL | 1 - 1 | NO |
|  | LocNum | int | 4 | NULL allowed |  |  |
|  | ElsDate | smalldatetime | 4 | NULL allowed |  |  |
|  | pos_staging_upload_id | int | 4 | NULL allowed |  |  |
| [![Cluster Primary Key PK_History_Payment_Signature_201910: Payment_Signature_ID](../../../../Images/pkcluster.png)](#indexes) | Payment_Signature_ID | int | 4 | NOT NULL |  |  |
|  | EOD_ID | int | 4 | NOT NULL |  |  |
|  | Shift_Num | int | 4 | NOT NULL |  |  |
|  | Trans_Num | int | 4 | NOT NULL |  |  |
|  | Payment_Num | int | 4 | NOT NULL |  |  |
|  | Signature | varchar(5632) | 5632 | NOT NULL |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

