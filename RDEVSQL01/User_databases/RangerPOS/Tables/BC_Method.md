#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Method

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Method]

---

## <a name="#columns"></a>Columns

| Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|
| Id | int | 4 | NOT NULL | 1 - 1 | NO |
| LocNum | int | 4 | NOT NULL |  |  |
| ElsDate | smalldatetime | 4 | NOT NULL |  |  |
| pos_staging_upload_id | int | 4 | NULL allowed |  |  |
| Method_ID | int | 4 | NOT NULL |  |  |
| Bill_Comp_ID | int | 4 | NULL allowed |  |  |
| Method_Name | varchar(20) | 20 | NULL allowed |  |  |
| Method_Code | varchar(15) | 15 | NULL allowed |  |  |
| Method_Type | varchar(15) | 15 | NULL allowed |  |  |
| Account_ID | int | 4 | NULL allowed |  |  |
| Fuel_Discount | money | 8 | NULL allowed |  |  |
| Fuel_Price | varchar(1) | 1 | NULL allowed |  |  |
| Is_Activated | bit | 1 | NULL allowed |  |  |
| SmartDESQ_Flag | bit | 1 | NULL allowed |  |  |
| Fee_Amount | money | 8 | NULL allowed |  |  |
| Fee_Account_ID | int | 4 | NULL allowed |  |  |
| Recharge_Account_ID | int | 4 | NULL allowed |  |  |
| BO_Code | varchar(40) | 40 | NULL allowed |  |  |
| BO_Subcode | varchar(40) | 40 | NULL allowed |  |  |
| Fee_Percent | money | 8 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

