#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.Data_Flag

# ![Tables](../../../../Images/Table32.png) [dbo].[Data_Flag]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability | Identity |
|---|---|---|---|---|---|
| [![Cluster Primary Key PK_Data_Flag: Data_Flag_ID](../../../../Images/pkcluster.png)](#indexes) | Data_Flag_ID | int | 4 | NOT NULL | 1 - 1 |
|  | Flag_Num | int | 4 | NULL allowed |  |
|  | Flag_Name | varchar(20) | 20 | NULL allowed |  |
|  | Display_Name | varchar(50) | 50 | NULL allowed |  |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |  |
|  | LocNum | int | 4 | NULL allowed |  |
|  | EOD_ID | int | 4 | NOT NULL |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

