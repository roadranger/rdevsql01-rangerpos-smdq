#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.Account

# ![Tables](../../../../Images/Table32.png) [dbo].[Account]

---

## <a name="#description"></a>MS_Description

SMDQ Account records by location from store level TRENDAR tables for reporting on History_*

## <a name="#columns"></a>Columns

| Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|
| Id | int | 4 | NOT NULL | 1 - 1 | NO |
| LocNum | int | 4 | NOT NULL |  |  |
| ElsDate | smalldatetime | 4 | NOT NULL |  |  |
| pos_staging_upload_id | int | 4 | NULL allowed |  |  |
| Account_ID | int | 4 | NOT NULL |  |  |
| Account_Name | varchar(15) | 15 | NULL allowed |  |  |
| Account_Long_Name | varchar(30) | 30 | NULL allowed |  |  |
| Acct_Type_ID | int | 4 | NULL allowed |  |  |
| Company_ID | int | 4 | NULL allowed |  |  |
| Acct_Group_ID | int | 4 | NULL allowed |  |  |
| Acct_Summary_ID | int | 4 | NULL allowed |  |  |
| Shift_Bal_ID | int | 4 | NULL allowed |  |  |
| PCATS_Code | varchar(6) | 6 | NULL allowed |  |  |
| Verification_Age | smallint | 2 | NULL allowed |  |  |
| Sunday_Start_Time | smalldatetime | 4 | NULL allowed |  |  |
| Sunday_End_Time | smalldatetime | 4 | NULL allowed |  |  |
| Report_Order | smallint | 2 | NULL allowed |  |  |
| Sales_Tax_ID | int | 4 | NULL allowed |  |  |
| Department | varchar(20) | 20 | NULL allowed |  |  |
| Min_Clerk_Age | smallint | 2 | NULL allowed |  |  |
| Prohibit_Open_Sales | bit | 1 | NULL allowed |  |  |
| Open_Limit_Low | decimal(15,2) | 9 | NULL allowed |  |  |
| Open_Limit_High | decimal(15,2) | 9 | NULL allowed |  |  |
| Status | varchar(1) | 1 | NULL allowed |  |  |
| Status_Date_Time | datetime | 8 | NULL allowed |  |  |
| Allow_WIC | bit | 1 | NULL allowed |  |  |
| Allow_Fraction_Qty | bit | 1 | NULL allowed |  |  |
| Force_Qty | bit | 1 | NULL allowed |  |  |
| Force_Weight | bit | 1 | NULL allowed |  |  |
| Prohibit_Discounts | bit | 1 | NULL allowed |  |  |
| Prohibit_EBT | bit | 1 | NULL allowed |  |  |
| Prohibit_Food_Stamp | bit | 1 | NULL allowed |  |  |
| Prohibit_Price_Lookup | bit | 1 | NULL allowed |  |  |
| Prohibit_Price_Override | bit | 1 | NULL allowed |  |  |
| Prohibit_Refund | bit | 1 | NULL allowed |  |  |
| Prohibit_Return | bit | 1 | NULL allowed |  |  |
| Prohibit_Tax_Modify | bit | 1 | NULL allowed |  |  |
| Require_Permit | bit | 1 | NULL allowed |  |  |
| Cash_Only | bit | 1 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

