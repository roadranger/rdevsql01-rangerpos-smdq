#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.Account_Blackouts

# ![Tables](../../../../Images/Table32.png) [dbo].[Account_Blackouts]

---

## <a name="#description"></a>MS_Description

SMDQ Account_Blackouts records by location from store level TRENDAR tables for reporting on History_*

## <a name="#columns"></a>Columns

| Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|
| Id | int | 4 | NOT NULL | 1 - 1 | NO |
| LocNum | int | 4 | NOT NULL |  |  |
| ElsDate | smalldatetime | 4 | NOT NULL |  |  |
| pos_staging_upload_id | int | 4 | NULL allowed |  |  |
| account_id | int | 4 | NOT NULL |  |  |
| day | int | 4 | NOT NULL |  |  |
| startbo | smalldatetime | 4 | NULL allowed |  |  |
| endbo | smalldatetime | 4 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

