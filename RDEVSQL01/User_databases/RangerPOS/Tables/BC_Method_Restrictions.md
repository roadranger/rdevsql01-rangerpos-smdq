#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Method_Restrictions

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Method_Restrictions]

---

## <a name="#columns"></a>Columns

| Key | Name | Data Type | Max Length (Bytes) | Nullability |
|---|---|---|---|---|
| [![Cluster Primary Key PK_BC_Method_Restrictions: Method_ID\PCATS_Code](../../../../Images/pkcluster.png)](#indexes) | Method_ID | int | 4 | NOT NULL |
| [![Cluster Primary Key PK_BC_Method_Restrictions: Method_ID\PCATS_Code](../../../../Images/pkcluster.png)](#indexes) | PCATS_Code | varchar(6) | 6 | NOT NULL |
|  | ELSDATE | smalldatetime | 4 | NULL allowed |
|  | LocNum | int | 4 | NULL allowed |
|  | EOD_ID | int | 4 | NOT NULL |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

