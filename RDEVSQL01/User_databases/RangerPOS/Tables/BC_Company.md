#### 

[Project](../../../../index.md) > [RDEVSQL01](../../../index.md) > [User databases](../../index.md) > [RangerPOS](../index.md) > [Tables](Tables.md) > dbo.BC_Company

# ![Tables](../../../../Images/Table32.png) [dbo].[BC_Company]

---

## <a name="#columns"></a>Columns

| Name | Data Type | Max Length (Bytes) | Nullability | Identity | Identity Replication |
|---|---|---|---|---|---|
| Id | int | 4 | NOT NULL | 1 - 1 | NO |
| LocNum | int | 4 | NOT NULL |  |  |
| ElsDate | smalldatetime | 4 | NOT NULL |  |  |
| pos_staging_upload_id | int | 4 | NULL allowed |  |  |
| Bill_Comp_ID | int | 4 | NOT NULL |  |  |
| Bill_Comp_Code | varchar(20) | 20 | NULL allowed |  |  |
| Bill_Comp_Abbrev | varchar(20) | 20 | NULL allowed |  |  |
| Bill_Comp_Name | varchar(30) | 30 | NULL allowed |  |  |
| Bill_Comp_Address_Line_1 | varchar(30) | 30 | NULL allowed |  |  |
| Bill_Comp_Address_Line_2 | varchar(30) | 30 | NULL allowed |  |  |
| Bill_Comp_City | varchar(20) | 20 | NULL allowed |  |  |
| Bill_Comp_State | varchar(2) | 2 | NULL allowed |  |  |
| Bill_Comp_Zip_Code | varchar(10) | 10 | NULL allowed |  |  |
| Bill_Comp_Phone | varchar(15) | 15 | NULL allowed |  |  |
| Bill_Comp_Fax | varchar(15) | 15 | NULL allowed |  |  |
| Bill_Comp_Merchant_1 | varchar(15) | 15 | NULL allowed |  |  |
| Bill_Comp_Merchant_2 | varchar(15) | 15 | NULL allowed |  |  |
| Bill_Comp_Terminal_ID | varchar(15) | 15 | NULL allowed |  |  |
| Bill_Comp_Dial_1 | varchar(15) | 15 | NULL allowed |  |  |
| Bill_Comp_Dial_2 | varchar(15) | 15 | NULL allowed |  |  |
| Bill_Comp_Baud | varchar(10) | 10 | NULL allowed |  |  |
| Bill_Comp_IP_1 | varchar(100) | 100 | NULL allowed |  |  |
| Bill_Comp_IP_2 | varchar(100) | 100 | NULL allowed |  |  |
| Bill_Comp_IP_Port_1 | varchar(6) | 6 | NULL allowed |  |  |
| Bill_Comp_IP_Port_2 | varchar(6) | 6 | NULL allowed |  |  |
| Bill_Comp_IP_Port_3 | varchar(6) | 6 | NULL allowed |  |  |
| Bill_Comp_Data_Bits | char(1) | 1 | NULL allowed |  |  |
| Bill_Comp_Stop_Bits | char(1) | 1 | NULL allowed |  |  |
| Bill_Comp_Parity | char(1) | 1 | NULL allowed |  |  |
| Is_Activated | bit | 1 | NULL allowed |  |  |
| Bill_Comp_Protocol | varchar(25) | 25 | NULL allowed |  |  |


---

###### Author:  Michelle Boyer

###### Copyright 2020 - All Rights Reserved

###### Created: Thursday, July 23, 2020 9:51:30 AM

